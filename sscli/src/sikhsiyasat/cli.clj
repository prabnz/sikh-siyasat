(ns sikhsiyasat.cli
  (:gen-class)
  (:require [clojure.tools.cli :as cli]
            [clojure.pprint :refer [pprint]]
            [image-resizer.resize :as resize]
            [image-resizer.rotate :as rotate]
            [image-resizer.format :as format]
            [image-resizer.scale-methods :as scale-methods]
            [image-resizer.crop :as crop]
            [clojure.java.io :as io]
            [cheshire.core :as cheshire]
            [clojure.string :as string]
            [clj-pdf.core :as clj-pdf])
  (:import (java.awt.image BufferedImage)
           (java.io File)
           (javax.imageio ImageIO)
           (java.awt Graphics2D)))


(set! *warn-on-reflection* true)


(defn image-file? [name]
  (or (string/ends-with? name ".JPG")
      (string/ends-with? name ".jpg")))


(defn concat-path [path1 path2]
  (str path1 "/" path2))


(defn ^File file [path]
  (let [f (io/file path)]
    (if (.exists f)
      f
      (println "file does not exists" f))))


(defn change-size [^BufferedImage image options]
  (let [ratio (double (:ratio options))]
    (if (and (< 0.0 ratio) (< ratio 100.0))
      (do
        (println "reducing size to " ratio "%")
        (let [new-width  (/ (* (.getWidth image) ratio) 100)
              new-height (/ (* (.getHeight image) ratio) 100)
              rz-fn      (resize/resize-fn new-width new-height scale-methods/ultra-quality)]
          (rz-fn image)))
      image)))


(defn crop-it [^BufferedImage image options]
  (let [{:keys [crop-left crop-right crop-bottom crop-top]} options]
    (if (or (< 0 crop-left) (< 0 crop-right) (< 0 crop-bottom) (< 0 crop-top))
      (do
        (println "cropping")
        (let [new-width  (- (.getWidth image) crop-left crop-right)
              new-height (- (.getHeight image) crop-top crop-bottom)
              cr-fn      (crop/crop-fn crop-left crop-top new-width new-height)]
          (cr-fn image)))
      image)))


(defn rotate [^BufferedImage image page-info options]
  (if (:rotate options)
    (if (even? (:page page-info))
      (do
        (println "rotating left")
        ((rotate/rotate-270-counter-clockwise-fn) image))
      (do
        (println "rotating right")
        ((rotate/rotate-90-counter-clockwise-fn) image)))
    image))


(defn save-new-image [image path]
  (if (nil? image)
    (println "image does not exists " path)
    (do
      (println "saving..." path)
      (io/make-parents path)
      (format/as-file image path :verbatim))))


(defn save-info-json [folder new-page-infos]
  (let [new-path (concat-path folder "info.json")
        images   (into [] (filter identity new-page-infos))
        info     {:folder-path folder
                  :images images}]
    (println "saving..." new-path)
    (io/make-parents folder)
    (spit new-path (cheshire/generate-string info {:pretty true}))))


(defn load-and-process-image [page-info options]
  (let [image-name (:name page-info)
        folder (:source-folder options)]
    (println "processing" page-info)
    (some->
        (concat-path folder image-name)
        (file)
        (ImageIO/read)
        (change-size options)
        (rotate page-info options)
        (crop-it options))))


(defn save-pdf [pdf-path images]
  (println "creating pdf " pdf-path)
  (io/make-parents pdf-path)
  (let [pages (->> images
                   (filter (fn [i] (not (nil? i))))
                   (map (fn [^BufferedImage page]
                          (let [scale (double (/ 2348 (.getWidth page)))]
                            [:graphics {:scale [scale scale]} (fn [^Graphics2D g2d] (.drawImage g2d page 0 0 nil))]))))
        with-page-breaks (interleave pages (repeat [:pagebreak]))]
    (clj-pdf/pdf [{:size :a0} with-page-breaks] pdf-path)))


(defn pages-for-pdf [image-infos options]
  (->> image-infos
       (sort-by (juxt :edition :page))
       (map (fn [i] (load-and-process-image i options)))))


(defn optimize-folder
  ([options]
   (let [source-folder (:source-folder options)
         ^File folder (io/file source-folder)]
     (println "processing folder" source-folder)
     (if (.isDirectory folder)
       (let [children-names (.list folder)
             folder-name (.getName folder)
             image-names (filter image-file? children-names)
             info-json-file (io/file (concat-path source-folder "info.json"))
             info (if (.exists info-json-file) (cheshire/parse-string (slurp info-json-file) true))
             target-sub-folder (concat-path (:target-folder options) folder-name)]
         (if (and (not-empty image-names) (not (nil? info)))
           (let [page-infos (sort-by (juxt :day :edition :page)(:images info))]
             (if (:pdf options)
               (do
                 (println "pdf: selected folder" source-folder)
                 (doseq [[page-number image-infos] (sort-by first (group-by :day page-infos))]
                   (let [pages (pages-for-pdf image-infos options)]
                     (save-pdf (str target-sub-folder "/" (.getName folder) "_" page-number ".pdf")
                               pages))))
               (let [new-page-infos (for [page-info page-infos]
                                      (let [image-number (format "%02d_%02d_%02d" (:day page-info) (:edition page-info) (:page page-info))
                                            new-image-name (str folder-name "_" image-number ".JPG")
                                            new-path (concat-path target-sub-folder new-image-name)]
                                        (if (.exists (io/file new-path))
                                          (println "already exists" new-path)
                                          (save-new-image (load-and-process-image page-info options) new-path))
                                        (assoc page-info :name new-image-name)))]
                 (save-info-json target-sub-folder new-page-infos))))
           (let [sub-folder-names (into [] (filter #(not (image-file? %)) children-names))]
             (println "sub-folders" sub-folder-names)
             (doseq [sub-folder sub-folder-names]
               (optimize-folder (assoc options :source-folder (concat-path source-folder sub-folder)))))))))))


(def cli-options
  ;; An option with a required argument
  [[nil "--help" "help"]
   [nil "--pdf" "To Generate a PDF document"]
   [nil "--optimise" "To optimise files"]
   [nil "--rotate" "to auto rotate files"]
   [nil "--source-folder source-folder" "Source Folder"]
   [nil "--target-folder target-folder" "Target Folder"]
   [nil "--ratio ratio" "Ratio"
    :default (double 100)
    :parse-fn #(Double/parseDouble %)]
   [nil "--crop-top crop-top" "Crop top value"
    :default (double 0)
    :parse-fn #(Double/parseDouble %)]
   [nil "--crop-bottom crop-bottom" "Crop bottom value"
    :default (double 0)
    :parse-fn #(Double/parseDouble %)]
   [nil "--crop-left crop-left" "Crop left value"
    :default (double 0)
    :parse-fn #(Double/parseDouble %)]
   [nil "--crop-right crop-right" "Crop right value"
    :default (double 0)
    :parse-fn #(Double/parseDouble %)]])


(defn print-errors [opt]
  (println "Sikh Siyasat CLI")
  (println "----------------")
  (println "")
  (println "Erorrs")
  (println "------")
  (println (:errors opt))
  (println "")
  (println "Options so far")
  (println "-------")
  (pprint (:options opt))
  (println "")
  (println "Summary")
  (println "--------")
  (println (:summary opt)))


(defn -main [& args]
  (let [opt (cli/parse-opts args cli-options)
        options (:options opt)]
    (cond
      (:errors opt) (print-errors opt)
      (:help options) (println (:summary opt))
      (and (:source-folder options) (:target-folder options)) (optimize-folder options)
      :else (do
              (println "Source Folder and Target Folder is required")
              (println (:summary opt))))))
