(defproject sscli "15"
  :description "Sikh Siyasat CLI"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [image-resizer "0.1.10"]
                 [cheshire "5.8.1"]
                 [clj-pdf "2.3.4"]
                 [org.clojure/tools.cli "0.4.2"]]
  :main ^:skip-aot sikhsiyasat.cli
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
