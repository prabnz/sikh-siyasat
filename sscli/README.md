# image-optimizer

This is a command line tool which runs on the jvm. Build output is a jar file which could be invoked as following

```bash
java -jar /Users/prabh/src/sikh-siyasat/sscli/target/uberjar/sscli-14-standalone.jar \
--source-folder /Users/prabh/Downloads/ \
--target-folder /Users/prabh/temp7






java -jar sscli-13.0-standalone.jar\ 
--source-folder "/Users/prabh/Downloads"\ 
--target-folder "/Users/prabh/temp2" \
--rotate \
--ratio 70 \
--crop-left 0 \  
--crop-right 0  \
--crop-top 0  --crop-bottom 0  \     
java -jar sscli-12.0-standalone.jar --source-folder "/Users/prabh/Downloads" --target-folder "/Users/prabh/temp2" --rotate
java -jar sscli-13.0-standalone.jar --source-folder "/Users/prabh/Downloads" --target-folder "/Users/prabh/temp2" --pdf --rotate --ratio 70 --crop-left 0  --crop-right 0 --crop-top 0  --crop-bottom 
java -jar sscli-13.0-standalone.jar --source-folder "/Users/prabh/Downloads" --target-folder "/Users/prabh/temp2" --pdf --rotate --ratio 70 
```

To build this project you will need leiningen 

then you can run 

```bash
lein uberjar 
```
 to create the jar file.

