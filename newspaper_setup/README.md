## How to Run
```bash
npm install
npx shadow-cljs watch renderer

#in another terminal
npx electron .
```

## Release
```bash
npx shadow-cljs compile renderer
npx electron-builder
```

## Other Commands
```bash
npm install

#dev
npx shadow-cljs watch renderer
npx electron .

#build
npx shadow-cljs compile renderer

#release
npx shadow-cljs release renderer

#clean
rm -rf resources/public/js/* && rm -rf target

#pack
npx electron-builder --dir

#dist
npx electron-builder
```

