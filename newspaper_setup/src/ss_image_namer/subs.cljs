
(ns ss-image-namer.subs
  (:require [re-frame.core :as re-frame]
            [ss-image-namer.util :as util]))

(re-frame/reg-sub
 :state
 (fn [db _] db))

(re-frame/reg-sub
 :selected-image
 (fn [db _]
   (util/get-selected-image db)))

(re-frame/reg-sub
 :error-messages
 (fn [db _]
   (->> db
        (:images)
        (group-by :day)
        (sort-by first)
        (reduce (fn [errors [day images]]
                  (cond
                    (> day 31)
                    (conj errors (str "Cannot have day " day " in a month"))

                    (odd? (count images))
                    (conj errors (str "Day " day " has " (count images) " pages, should be even"))

                    :else
                    errors))
                []))))



