(ns ss-image-namer.db
  (:require [cljs.reader]
            [cljs.spec.alpha :as s]))

(s/def ::folder-path string?)

(s/def ::index int?)

(s/def ::day int?)

(s/def ::edition int?)

(s/def ::page int?)

(s/def ::image (s/keys :req [::index ::name ::day ::edition ::page]))

(s/def ::images (s/coll-of ::image))

(s/def ::selected-image-index int?)

(s/def ::db (s/keys :reg [::image ::folder-path]
                    :opt [::selected-image-index]))

(def default-db
  {:folder-path ""
   :images []
   :selected-image-index 1})
