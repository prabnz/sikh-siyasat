(ns ss-image-namer.views
  (:require [re-frame.core :as re-frame]
            [ss-image-namer.util :as util]
            [ss-image-namer.views.image-detail :refer [image-detail]]
            [ss-image-namer.views.nav-bar :refer [nav-bar]]))

(defn list-item [image is-selected]
  (let [day        (:day image)
        index      (:index image 0)
        page       (:page image 0)
        front-page (util/front-page? image)
        edition    (:edition image)
        supplement (> edition 0)]
    [:div {:class    (str "item list-item"
                          " "
                          (if is-selected "selected" "")
                          " "
                          (if (odd? index) "odd-index" "even-index")
                          " "
                          (if front-page "front-page" ""))
           :style {:padding "0"
                   :width "80px;"}
           :on-click #(re-frame/dispatch [:set-selected-image (:index image)])
           :key      (:index image)}
     [:input {:read-only true
              :style { :height "17px";
                       :border "0px;"
                       :width "100%;"
                       :margin "0px;"
                       :padding-left "8px"}
              :value     (str
                          day
                          "_"
                          (if supplement "s" "") "_"
                          page
                          (if (:place-holder image) "_v" ""))}]]))

(defn page-list [images selected-image]
  [:div
   [:div {:class "image-list ui celled list"
          :style {:height "800px;"
                  :margin "0px;"
                  :padding-left "5px;"
                  :padding-right "5px;"
                  :overflow-y "scroll;"
                  :list-style-type "none;"
                  :min-width "100px;"}}
    (map #(list-item %1 (= %1 selected-image)) images)]])

(defn root-component []
  (let [db             (re-frame/subscribe [:state])
        state          @db
        folder-path    (:folder-path state)
        images         (:images state)
        selected-image (util/get-selected-image state)
        error-message  (first @(re-frame/subscribe [:error-messages]))]
    (println "selected-image" selected-image)
    [:div
     [nav-bar selected-image]
     (if-not (#{nil ""} folder-path)
       [:p
        folder-path
        "/"
        [:b {:style {:background-color "#f6e58d"}} (:name selected-image)]
        "total/"
        (count images)])
     (when error-message 
       [:div {:class "ui negative message"} error-message])
     [:div {:style       {:display "flex"}
            :on-key-down (fn [event]
                           (.preventDefault event)
                           (let [key-code (.-keyCode event)]
                             (re-frame/dispatch [:on-key-down key-code])))}
      [page-list images selected-image]
      (if selected-image
        [image-detail folder-path selected-image]
        [:div "nothing-selected"])]]))
