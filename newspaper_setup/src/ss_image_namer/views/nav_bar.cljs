(ns ss-image-namer.views.nav-bar
  (:require [re-frame.core :as re-frame]
            [ss-image-namer.util :as util]))

(defn nav-bar [selected-image]
  [:div {:class "ui tiny buttons top attached"}
   [:button {:on-click #(re-frame/dispatch [:choose-folder])
             :class "ui button tiny"}
    "Open"]
   [:button {:on-click #(re-frame/dispatch [:save])
             :class "ui button tiny"}
    "Save"]
   [:button {:on-click #(re-frame/dispatch [:toggle-front-page selected-image])
             :class    (str "ui button")
             :title    "Toggle Front Page"}
    "Front"]
   [:button {:on-click #(re-frame/dispatch [:toggle-supplement selected-image])
             :class "ui button tiny"
             :title    "Toggle Supplement"}
    "Sup"]
   [:button {:on-click #(re-frame/dispatch [:move-day-up selected-image])
             :class    (str "ui button tiny " (if selected-image "" "disabled"))
             :title    "Move day up"}
    "D\u2191"]
   [:button {:on-click #(re-frame/dispatch [:move-day-down selected-image])
             :class    (str "ui button tiny " (if selected-image "" "disabled"))
             :title    "Move day down"}
    "D\u2193"]
   [:button {:on-click #(re-frame/dispatch [:change-day selected-image inc])
             :class    (str "ui button tiny " (if (util/front-page? selected-image) "" "disabled"))
             :title    "Add day"}
    "D+"]
   [:button {:on-click #(re-frame/dispatch [:change-day selected-image dec])
             :class    (str "ui button tiny " (if (util/front-page? selected-image) "text-info" "disabled"))
             :title    "Remove day"}
    "D-"]
   [:button {:on-click #(re-frame/dispatch [:move-up selected-image])
             :class    (str "ui button tiny " (if selected-image "" "disabled"))
             :title    "Move page up"}
    "P\u2191"]
   [:button {:on-click #(re-frame/dispatch [:move-down selected-image])
             :class    (str "ui button tiny " (if selected-image "" "disabled"))
             :title    "Move page down"}
    "P\u2193"]
   [:button {:on-click #(re-frame/dispatch [:add-image-after selected-image])
             :class "ui button tiny"
             :title    "Add Page After"}
    "P+"]
   [:button {:on-click #(re-frame/dispatch [:add-image-before selected-image])
             :class "ui button tiny"
             :title    "Add Page Before"}
    "+P"]
   [:button {:on-click (fn [] (when (js/confirm "Sure remove?"))
                         (re-frame/dispatch [:remove-image selected-image]))
             :class "ui button tiny"
             :title    "Remove Page"}
    "P-"]])
