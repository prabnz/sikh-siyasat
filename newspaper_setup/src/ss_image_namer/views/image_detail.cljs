(ns ss-image-namer.views.image-detail
  (:require [goog.object :as goog.object]))

(defn show-zoom-result [e]
  (let [event             (if (nil? e) (.-event js/window) e)
        zoom-ratio        2

        img               (.getElementById js/document "image")
        img-width         (.-offsetWidth img)
        img-height        (.-offsetHeight img)

        zoom              (.getElementById js/document "zoom-result")
        zoom-width        (.-offsetWidth zoom)
        zoom-height       (.-offsetHeight zoom)

        img-rect          (.getBoundingClientRect img)

        x                 (- (.-pageX event) (.-pageXOffset js/window) (.-left img-rect))
        y                 (- (.-pageY event) (.-pageYOffset js/window) (.-top img-rect))

        x-pos             (js/Math.max (js/Math.min (- x (/ zoom-width 2)) (- img-width zoom-width)) 0)
        y-pos             (js/Math.max (js/Math.min (- y (/ zoom-height 2)) (- img-height zoom-height)) 0)

        x-background-size (* img-width zoom-ratio)
        y-background-size (* img-height zoom-ratio)

        x-background-pos  (+ (* x zoom-ratio -1) (/ zoom-width 2))
        y-background-pos  (+ (* y zoom-ratio -1) (/ zoom-height 2))

        style             (.-style zoom)]

    (.preventDefault event)

    (goog.object/set style "backgroundSize" (str x-background-size "px " y-background-size "px"))
    (goog.object/set style "backgroundPosition" (str x-background-pos "px " y-background-pos "px"))
    (goog.object/set style "left" (str x-pos "px"))
    (goog.object/set style "top" (str y-pos "px"))
    (goog.object/set style "display" "block")))

(defn on-mouse-out [_e]
  (-> js/document
      (.getElementById "zoom-result")
      (.-style)
      (goog.object/set "display" "none")))

(defn image-detail [folder-path selected-image]
  (let [src (str folder-path "/" (:name selected-image))]
    [:div {:style {:position "relative"}}
     [:div {:id            "zoom-result"
            :style         {:background-image (str "url('" src "')")
                            :border           "0.5px solid black"
                            :width            "250px"
                            :height           "250px"
                            :position         "absolute"
                            :display          "none"
                            :left             0
                            :top              0}
            :on-mouse-move show-zoom-result
            :on-touch-move show-zoom-result
            :on-mouse-out  on-mouse-out}]

     [:img {:id            "image"
            :src           src
            :on-mouse-move show-zoom-result
            :on-touch-move show-zoom-result
            :style {:width "100%;"}}]]))
