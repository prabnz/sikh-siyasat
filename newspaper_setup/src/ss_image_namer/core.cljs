(ns ss-image-namer.core
  (:require [reagent.core :as reagent]
            [re-frame.core :refer [dispatch-sync]]
            [ss-image-namer.events] ;; These two are only required to make the compiler
            [ss-image-namer.subs]   ;; load them
            [ss-image-namer.views :as views]))

(defn ^:dev/after-load start []
  (reagent/render [views/root-component] (js/document.getElementById "app-container")))


(defn init []
  (dispatch-sync [:initialise-db])
  (start))

