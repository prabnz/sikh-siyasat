(ns ss-image-namer.util)

(defn front-page? [image]
  (and
   (= 1 (:page image 0))
   (= 0 (:edition image))))

(defn can-toggle-front-page [image]
  true)

(defn get-selected-image [db]
  (let [images (:images db)
        selected-image-index (:selected-image-index db)]
    (first (filter #(= selected-image-index (:index %1)) images))))
