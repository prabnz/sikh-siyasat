(ns ss-image-namer.events
  (:require
   [ss-image-namer.db :as ss-image-namer.db]
   [ss-image-namer.util :as util]
   [re-frame.core :as re-frame]
   [cljs.spec.alpha :as s]
   [clojure.string :as string]))

(defonce electron (js/require "electron"))
(defonce fs (js/require "fs"))

(defn- safe [op]
  (try
    (op)
    (catch :default e
      (println "error" e)
      nil)))

(defn- jpg? [name]
  (or
   (string/ends-with? name ".JPG")
   (string/ends-with? name ".jpg")))

(defn- split-image-name [name]
  (-> name
      (string/replace ".jpg" "")
      (string/split "-")))

(defn- parse-page [index name]
  (let [parts (split-image-name name)
        index (inc index)]
    (if (= 3 (count parts))
      (let [[day edition page] parts]
        {:name    name
         :day     (js/parseInt day)
         :edition (js/parseInt edition)
         :page    (js/parseInt page)
         :index   index})
      {:name    name
       :day     1
       :edition 0
       :page    (if (= index 1) 1 2)
       :index   index})))

(defn parse-pages [paths]
  (->> paths
       (filter jpg?)
       (sort)
       (map-indexed parse-page)))

(defn log [a b]
  (println a b)
  b)

(defn get-page-number [idx total]
  (let [half (/ total 2)]
    (if (< idx half)
      (inc (* idx 2)) ;first the pages go up like 1 2 3 5 7
      (- total (* (- idx half) 2))))) ; then they go down like 10 8 6 4 2

(defn set-pages-in-day [images-in-day]
  (let [total (count images-in-day)]
    (map-indexed (fn [i item]
                   (assoc item :page (get-page-number i total)))
                 images-in-day)))

(defn set-pages [images]
  (println "set-pages" images)
  (let [days           (group-by (juxt :day :edition) images)
        with-pages-set (mapcat (fn [[_k v]] (set-pages-in-day v)) days)
        sorted         (sort-by :index with-pages-set)]
    (vec sorted)))

(defn check-and-throw
  "Throws an exception if `db` doesn't match the Spec `a-spec`."
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

(def check-spec-interceptor (re-frame/after (partial check-and-throw :ss-image-namer.db/db)))

;; -- Event Handlers ----------------------------------------------------------

(re-frame/reg-event-fx                 ;; part of the re-frame API
 :initialise-db              ;; event id being handled

 [check-spec-interceptor]           ;; after the event handler runs, check app-db matches Spec

 (fn [{:keys [db _local-store-todos]} _]                    ;; take 2 vals from coeffects. Ignore event vector itself.
   (if (nil? db)
     {:db ss-image-namer.db/default-db}     ;; all hail the new state
     {:db db})))

(re-frame/reg-event-db
 :set-selected-image
 [check-spec-interceptor]
 (fn [db [_ index]]
   (assoc db :selected-image-index index)))

(defn change-day [selected-image images operation]
  (let [index (:index selected-image)
        day   (:day selected-image)]
    (->> images
         (map (fn [img]
                (if (>= (:index img) index)
                  (let [was-same-day (= (:day img) day)
                        day-updated  (update-in img [:day] operation)]
                    (if was-same-day
                      (assoc day-updated :edition 0)
                      day-updated))
                  img)))
         (set-pages))))

(re-frame/reg-event-db
 :change-day
 [check-spec-interceptor]
 (fn [db [_ selected-image operation]]
   (if (util/front-page? selected-image)
     (assoc db :images (change-day
                        selected-image
                        (:images db)
                        operation))
     db)))

(re-frame/reg-event-db
 :toggle-front-page
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (if (util/can-toggle-front-page selected-image)
     (assoc db :images (change-day
                        selected-image
                        (:images db)
                        (if (util/front-page? selected-image) dec inc)))  ; if it was the first page then merge it to the previous page
     db)))

(defn toggle-supplement [selected-image images]
  (let [index       (:index selected-image)
        edition     (:edition selected-image 0)
        new-edition (if (> edition 0) 0 1)]
    (->> images
         (map (fn [img]
                (if (= (:index img) index)
                  (assoc img :edition new-edition)
                  img)))
         (set-pages))))

(re-frame/reg-event-db
 :toggle-supplement
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (assoc db :images (toggle-supplement selected-image (:images db)))))

(defn insert [images position image]
  (concat (take position images) [image] (drop position images)))

(defn re-index [images]
  (vec (map-indexed
        (fn [idx img]
          (assoc img :index (inc idx)))
        images)))

(defn insert-image [index selected-image images]
  (let [image-to-insert  (assoc selected-image :index index
                                :name "place-holder"
                                :place-holder true)
        zero-based-index (dec index)
        new-images       (insert images zero-based-index image-to-insert)
        re-indexed       (re-index new-images)
        pages-set        (set-pages re-indexed)]
    pages-set))

(re-frame/reg-event-db
 :add-image-after
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (if (nil? selected-image)
     db
     (let [new-index (inc (:index selected-image))
           images    (:images db)]
       (assoc db :images (insert-image new-index selected-image images))))))

(re-frame/reg-event-db
 :add-image-before
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (if (nil? selected-image)
     db
     (let [index  (:index selected-image)
           images (:images db)]
       (assoc db :images (insert-image index selected-image images))))))

(re-frame/reg-event-db
 :remove-image
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (if (nil? selected-image)
     db
     (let [images     (:images db)
           index      (:index selected-image)
           new-images (remove (fn [img] (= (:index img) index)) images)
           re-indexed (re-index new-images)]
       (assoc db :images (set-pages re-indexed))))))

(defn swap-images [images i1 i2]
  (let [images  (vec images)
        image-1 (get images i1)
        image-2 (get images i2)]
    (if (or (nil? image-1) (nil? image-2))
      images
      (-> images
          (assoc i1 (assoc image-2 :day (:day image-1))
                 i2 (assoc image-1 :day (:day image-2)))
          (re-index)
          (set-pages)))))

(re-frame/reg-event-db
 :move-up
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (if (nil? selected-image)
     db
     (let [images           (:images db)
           index            (:index selected-image)
           zero-based-index (dec index)
           new-images       (swap-images images (dec zero-based-index) zero-based-index)]
       (assoc db :images new-images
              :selected-image-index (dec index))))))

(re-frame/reg-event-db
 :move-down
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (if (nil? selected-image)
     db
     (let [images           (:images db)
           index            (:index selected-image)
           zero-based-index (dec index)
           new-images       (swap-images images zero-based-index (inc zero-based-index))]
       (assoc db :images new-images
              :selected-image-index (inc index))))))

(defn swap-days [images day1]
  (let [day2       (inc day1)
        new-images (concat
                    (filter (fn [i] (< (:day i) day1)) images)
                    (->> images
                         (filter (fn [i] (= (:day i) day2)))
                         (map (fn [i] (assoc i :day day1))))
                    (->> images
                         (filter (fn [i] (= (:day i) day1)))
                         (map (fn [i] (assoc i :day day2))))
                    (filter (fn [i] (> (:day i) day2)) images))]
    (set-pages (re-index new-images))))

(defn find-index-for-day [day images]
  (let [img (first (filter #(= day (:day %1)) images))]
    (if (nil? img)
      0
      (:index img))))

(re-frame/reg-event-db
 :move-day-up
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (if (nil? selected-image)
     db
     (let [images     (:images db)
           day1       (dec (:day selected-image))
           new-images (swap-days images day1)]
       (assoc db :images new-images
              :selected-image-index (find-index-for-day day1 new-images))))))

(re-frame/reg-event-db
 :move-day-down
 [check-spec-interceptor]
 (fn [db [_ selected-image]]
   (if (nil? selected-image)
     db
     (let [images     (:images db)
           day1       (:day selected-image)
           new-images (swap-days images day1)]
       (assoc db :images new-images
              :selected-image-index (find-index-for-day (inc day1) new-images))))))

(re-frame/reg-event-db
 :choose-folder
 [check-spec-interceptor]
 (fn [db _]
   (let [dialog-options  (clj->js {:properties [:openDirectory]})
         result          (.showOpenDialogSync (.-dialog (.-remote electron)) dialog-options)
         new-folder-path (when result (aget result 0))]
     (when new-folder-path
       (re-frame/dispatch [:set-folder-path new-folder-path])))
   db))

(defn config-file-path [folder-path]
  (str folder-path "/info.json"))

(defn- read-image-paths [folder-path]
  (safe #(.readdirSync fs folder-path)))

(defn- read-config [folder-path]
  (safe (fn []
          (let [content (.readFileSync fs (config-file-path folder-path))
                json    (js/JSON.parse content)]
            (js->clj json :keywordize-keys true)))))

(re-frame/reg-event-db
 :set-folder-path
 [check-spec-interceptor]
 (fn [db [_ folder-path]]
   (if folder-path
     (let [config (read-config folder-path)]
       (assoc db :folder-path folder-path
              :images (if config
                        (do
                          (println "config" config)
                          (:images config))
                        (parse-pages (read-image-paths folder-path)))
              :selected-image-index 1))
     db)))

(defn save-file [db]
  (let [data      (clj->js db)
        content   (js/JSON.stringify data nil 4)
        file-name (str (:folder-path db) "/info.json")]
    (.writeFile fs file-name content
                (fn [error]
                  (if error
                    (do
                      (js/alert "Something went wrong")
                      (println "something went wrong" error))
                    (js/alert "Saved"))))))

(re-frame/reg-event-db
 :save
 [check-spec-interceptor]
 (fn [db _]
   (save-file db)
   db))

(re-frame/reg-event-db
 :on-key-down
 [check-spec-interceptor]
 (fn [db [_ key-code]]
   (let [selected-image       (util/get-selected-image db)
         selected-image-index (:index selected-image 1)]
     (case key-code
        ; f key
       70
       (re-frame/dispatch [:toggle-front-page selected-image])

        ; down arrow
       40
       (re-frame/dispatch [:set-selected-image (inc selected-image-index)])

        ; up arrow
       38
       (re-frame/dispatch [:set-selected-image (dec selected-image-index)])

          ; s key
       83
       (re-frame/dispatch [:toggle-supplement selected-image])

          ; i 
       73
       (re-frame/dispatch [:change-day selected-image inc])

          ; d
       68
       (re-frame/dispatch [:change-day selected-image dec])

       (js/alert (str key-code))))
   db))
