# image_optimizer UI

this is a very simple ui which invokes the image-optimizer command line app.  
look in `index.js` and `index.html` for the code.


```shell
# to develop
npm install electron -g 
electron .         

## to release
npm install electron-packager -g                                           
electron .
electron-packager . ImageOptimizerUI --platform=darwin --arch=x64 --electronVersion=1.7.10 --overwrite
```