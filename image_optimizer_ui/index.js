"use strict";
// @ts-check
var electron = require("electron");
var child_process = require("child_process");
const path = require('path');
var logs = [];

window.onload = function () {
    function selectFolder() {
        var result = electron.remote.dialog.showOpenDialog({ "properties": ["openDirectory"] })
        return result && result[0];
    }

    function selectFile() {
        var result = electron.remote.dialog.showOpenDialog({ "properties": ["openFile"] })
        return result && result[0];
    }

    optimizer_jar_file_path_button.onclick = function () {
        var result = selectFile();
        if (result) {
            optimizer_jar_file_path_input.value = result;
        }
    }

    source_folder_button.onclick = function () {
        var result = selectFolder();
        if (result) {
            source_folder_input.value = result;
            target_folder_input.value = "";
        }
    };

    target_folder_button.onclick = function () {
        var result = selectFolder();
        if (result) {
            target_folder_input.value = result;
        }
    };

    function getStringValue(field){
        if(field){
            return JSON.stringify(field.value);
        }
        return "";
    }

    process_button.onclick = function () {
        var src = getStringValue(source_folder_input);
        var trg = getStringValue(target_folder_input);

        if (src && trg) {
            var ratio = ratio_input.value || 70;
            var jarFilePath = getStringValue(optimizer_jar_file_path_input);

            var args = ["-jar",
                jarFilePath,
                src,
                trg,
                ratio,
                top_crop_input.value,
                bottom_crop_input.value,
                left_crop_input.value,
                right_crop_input.value];

            log(`calling java ${args.join(" ")}`);

            const ls = child_process.spawn('java', args, { shell: true });

            ls.stdout.on('data', (data) => {
                log(`${data}`);
            });

            ls.stderr.on('data', (data) => {
                log(`ERROR: ${data}`);
            });

            ls.on('close', (code) => {
                log(`child process exited with code ${code}`);
                alert("done. check logs to confirm")
            });

        } else {
            alert("please fill the Source and Target folders first")
        }
    }

    function addLog(logs, message) {
        var log = { message: message };
        if (logs.length >= 10) {
            logs.shift();
        }
        logs.push(log);
    }

    function renderLogs(logs) {
        output_log.innerHTML = "";
        for (var i = 0; i < logs.length; i++) {
            var el = createElement("li", logs[i].message);
            output_log.appendChild(el);
        }
    }

    function log(message) {
        console.log(message);
        addLog(logs, message);
        renderLogs(logs);
    }

    function createElement(tagName, content) {
        var d = document.createElement(tagName);
        d.innerHTML = content;
        return d;
    }
};
