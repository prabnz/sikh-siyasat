# ss-archive
 To get an interactive development environment run:

```bash
 clojure -A:fig:dev -m figwheel.main -b dev -r
```

 To create a production build run:

```bash
rm -rf target/public
clojure -A:fig:min -m figwheel.main -O advanced -bo dev
```

        lein ring server
