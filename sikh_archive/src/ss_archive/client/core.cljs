(ns ^:figwheel-hooks ss-archive.client.core
  (:require [goog.dom :as gdom]
            [reagent.core :as reagent]
            [re-frame.core :refer [dispatch dispatch-sync]]
            [ss-archive.client.events] ;; These two are only required to make the compiler
            [ss-archive.client.subs]   ;; load them
            [ss-archive.client.views :as views]))

(dispatch-sync [:initialise-db])

(defn ^:after-load  on-load []
  (when-let [el (gdom/getElement "app")]
    (reagent/render-component [views/layout] el)))

(on-load)
