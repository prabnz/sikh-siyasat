(ns ss-archive.client.events
  (:require
   [ss-archive.client.db]
   [re-frame.core :as re-frame]
   [cljs.spec.alpha :as s]))

(defn check-and-throw
  "Throws an exception if `db` doesn't match the Spec `a-spec`."
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

(def check-spec-interceptor (re-frame/after (partial check-and-throw :ss-archive.client.db/db)))

;; -- Event Handlers ----------------------------------------------------------

(re-frame/reg-event-db
 :previous-month
 [check-spec-interceptor]
 (fn [db [_]]
   (assoc db :month (dec (get db :month)))))

(re-frame/reg-event-db
 :next-month
 [check-spec-interceptor]
 (fn [db [_]]
   (assoc db :month (inc (get db :month)))))

(re-frame/reg-event-db
 :previous-day
 [check-spec-interceptor]
 (fn [db [_]]
   (assoc db :day (dec (get db :day)))))

(re-frame/reg-event-db
 :next-day
 [check-spec-interceptor]
 (fn [db [_]]
   (assoc db :day (inc (get db :day)))))

(re-frame/reg-event-db
 :previous-page
 [check-spec-interceptor]
 (fn [db [_]]
   (assoc db :page (dec (get db :page)))))

(re-frame/reg-event-db
 :next-page
 [check-spec-interceptor]
 (fn [db [_]]
   (assoc db :page (inc (get db :page)))))
