(ns ss-archive.client.views
  (:require [re-frame.core :refer [dispatch]]))

(defn image-card [url]
  [:img {:src "img/IMG_0894.jpg"}])

(defn mini-btn [{:keys [icon-name on-click]} children]
  [:button {:class-name "mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored mdl-js-ripple-effect"
            :on-click on-click
            :title "hello"}
   (when icon-name [:i.material-icons icon-name])
   children])

(defn layout []
  [:div#top-div.mdl-layout.mdl-js-layout.mdl-layout--fixed-header
   [:header.mdl-layout__header
    [:div.mdl-layout__header-row
     [:span.mdl-layout-title "Sikh Siyasat"]
     [:div.mdl-layout-spacer]
     [:nav.mdl-navigation
      [mini-btn {:title "Previous Month"
                 :icon-name "chevron_left"
                 :on-click #(dispatch [:previous-month])}]
      [mini-btn {:title "Previous Day"
                 :icon-name "chevron_left"
                 :on-click #(dispatch [:previous-day])}]
      [mini-btn {:title "Previous Page"
                 :icon-name "chevron_left"
                 :on-click #(dispatch [:previous-page])}]
      [mini-btn {:title "Next Page"
                 :icon-name "chevron_right"
                 :on-click #(dispatch [:next-page])}]
      [mini-btn {:title "Next Day"
                 :icon-name "chevron_right"
                 :on-click #(dispatch [:next-day])}]
      [mini-btn {:title "Next Month"
                 :icon-name "chevron_right"
                 :on-click #(dispatch [:next-month])}]]]]

   [:div.mdl-layout__drawer
    [:nav.mdl-navigation
     [:a#submenu.mdl-navigation__link {:href "#"} "Link"]
     [:ul.mdl-menu.mdl-menu--bottom-left.mdl-js-menu.mdl-js-ripple-effect
      {:for "submenu"}
      [:li.mdl-menu__item "Some Action"]
      [:li.mdl-menu__item "Another Action"]
      [:li.mdl-menu__item {:disabled "disabled"} "Disabled Action"]
      [:li.mdl-menu__item "Yet Another Action"]]
     [:a.mdl-navigation__link {:href ""} "Link"]
     [:a.mdl-navigation__link {:href ""} "Link"]
     [:a.mdl-navigation__link {:href ""} "Link"]]]
   [:main.mdl-layout__content
    [image-card]]])
