(ns ss-archive.server.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.file :refer [wrap-file]]
            [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clojure.data.json :as json])
  (:import (java.io PushbackReader)))

(defroutes app-routes
  (GET "/" [] "Hello World")
  (route/not-found "Not Found"))


(def config (-> "./resources/ss_archive_config.json"
                (slurp)
                (json/read-str :key-fn keyword)))


(def app
  (as-> app-routes ap
    (if (contains? config :resource-folder) (wrap-file ap (:resource-folder config)) ap)
    (wrap-defaults ap site-defaults)))
